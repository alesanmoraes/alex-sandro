<?php
include_once './controller/alunoController.php';
?>
<!doctype html>
<html lang="pt-br">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>

    <link href="./css/layout.css" rel="stylesheet">
  </head>
  <body class="bg-light">

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="index.php">TecnoFit Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      </div>
    </nav>

    <main role="main" class="container">

  <div class="position-relative p-1 p-md-5 m-md-3">
    <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Atualizar aluno</h4>
      <form class="needs-validation" novalidate action="atualizar_aluno.php" method="POST">
      <input type="hidden" name="id" value="<?=$_GET['id']?>" />
<?php    


$pesquisaAlunos = new alunoController();   

$pesquisaAlunos->setId($_GET['id']);
$retorno = $pesquisaAlunos->editarAluno(); 
foreach ($retorno as $infoUser){ 
?>  

              <div class="mb-3">
                <label for="name">Nome</label>
                <input type="text" class="form-control" id="name" name="name" value="<?=$infoUser['nome']?>" placeholder="" required>
                
              </div>            
            

            <div class="mb-3">
              <label for="email">E-mail</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</span>
                </div>
                <input type="text" class="form-control" id="email" name="email" value="<?=$infoUser['email']?>" placeholder="you@example.com" required>               
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="telefone">Telefone</label>
                
                <input type="text" class="form-control" id="telefone" name="telefone"  value="<?=$infoUser['telefone']?>" placeholder="Telefone" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="Sexo">Sexo</label>
                <select name="sexo" class="form-control">

              <?php
                if ($infoUser['sexo'] == 1) {
                  $desc = "Masculino";
                } else {
                  $desc = "Feminino";
                }
              ?>
                <option value="1" selected><?=$desc?></option>
                
                </select>

              </div>
            </div>

            <div class="mb-3">
              <label for="cpf">Cpf</label>
              <input type="text" class="form-control" id="cpf" name="cpf" value="<?=$infoUser['cpf']?>" placeholder="1234 Main St" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="endereco">Endereço</label>
              <input type="text" class="form-control" id="endereco" name="endereco" value="<?=$infoUser['endereco']?>" placeholder="1234 Main St" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="numero">Número</label>
                <input type="text" class="form-control" id="numero" name="numero" value="<?=$infoUser['numero']?>" placeholder="" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="complemento">Complemento</label>
                <input type="text" class="form-control" id="complemento" name="complemento" value="<?=$infoUser['complemento']?>" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

            <div class="mb-3">
              <label for="information">Outras informações</label>
              <textarea cols="50" rows="2" class="form-control" id="information" name="information" placeholder="Outras informações"><?=$infoUser['informacoes']?></textarea>
            </div>
         <?php } ?>
            <hr class="mb-4">
            
            <button class="btn btn-primary btn-lg btn-block" type="submit">Atualizar aluno</button>
          </form>
            
        </div>

  </div></main>

  <footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
  
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>