<?php
require "./controller/treinoController.php";

$atualizaAluno = new treinoController();

$atualizaAluno->setId_dest($_POST['cod_dest']); 
$atualizaAluno->setId_trei($_POST['cod_trei']);

if ($_POST['status_treino'] != 2 && $_POST['finalizado'] != 1) {
  $atualizaAluno->setId($_POST['status_treino']);
  $atualizaAluno->setsessao($_POST['finalizado']); 
  $atualizaAluno->setAtivo(1);

} elseif ($_POST['status_treino'] == 2 && $_POST['finalizado'] == 1) {

  $atualizaAluno->setId(2);
  $atualizaAluno->setsessao(2);
  $atualizaAluno->setAtivo(2);
} elseif ($_POST['status_treino'] == 1 && $_POST['finalizado'] == 2) {

  $atualizaAluno->setId(2);
  $atualizaAluno->setsessao(2);
  $atualizaAluno->setAtivo(2);
} else {
  
  $atualizaAluno->setId($_POST['status_treino']); //exercicio
  $atualizaAluno->setsessao($_POST['finalizado']);//pular exercicio
  $atualizaAluno->setAtivo(2);
}
  
$atualizaAluno->setId($_POST['status_treino']);
$atualizaAluno->setExercicio($_POST['treino']);// treino
$atualizaAluno->setRepeticao($_POST['repeticao']);//sessão
$atualizaAluno->setInstrutor($_POST['instrutor']);// instrutor

$atualizaAluno->setId_exec($_POST['cod_exer']);// treino
$atualizaAluno->setId_alu($_POST['cod_alu']);

$atualizaAluno->atualiza_treino(); 


header('location: ./index.php'); 


