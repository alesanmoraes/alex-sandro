<?php
session_start();
include_once './controller/exercicioController.php';

$pesquisaExercicio = new exercicioController();

$pesquisaExercicio->setId($_GET['id']);

$info = $pesquisaExercicio->pesquisaExercicioAtivo();

print_r ($info);
if ($info == true) {
  header("Location: index.php?treino=1");
  die();
}


$_SESSION['id_usuario'] = $_GET['id'];
$_SESSION['nome_usuario'] = $_GET['nome'];

//$_SESSION['id_exer'] = $_GET['id_exer'];
//$_SESSION['nome_exer'] = $_GET['nome_exer'];

?>


<!doctype html>
<html lang="pt-br">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>

    <link href="./css/layout.css" rel="stylesheet">
  </head>
  <body class="bg-light">

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="index.php">TecnoFit Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      </div>
    </nav>

    <main role="main" class="container">

  <div class="position-relative p-1 p-md-5 m-md-3">
    <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Cadastrar treino</h4>
          <form class="needs-validation" novalidate action="criar_treino.php" method="POST">
          <input type="hidden" name="Id_usuario" value="<?=$_SESSION['id_usuario']?>" />
            
              <div class="mb-3">
                <label for="name">Nome aluno</label>
                <input type="text" class="form-control" id="name" name="name" value="<?=$_SESSION['nome_usuario']?>" placeholder="" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>            
            
              <div class="mb-3">
                <label for="exercicio">Exercício</label>
                <select name="exercicio" class="form-control">
                  <?php
                    

                  $retorno = $pesquisaExercicio->pesquisaTodosExercicio(); 

                  foreach ($retorno as $infoUser){     
                      
                    ?><option value="<?= $infoUser['id'];?>"><?= $infoUser['descricao'];?></option>
                  <?php }?>
               
                </select>

              </div> 

              <div class="mb-3">
                <label for="treino">Descrição treino</label>
                <select name="treino" class="form-control">
                  <?php

                include_once './controller/treinoController.php';
               
                  $pesquisaTreino = new treinoController();   

                  $retornoTre = $pesquisaTreino->buscarAllTreino(); 

                  foreach ($retornoTre as $infoTreino){     
                      
                    ?><option value="<?= $infoTreino['id'];?>"><?= $infoTreino['descricao'];?></option>
                  <?php } ?>
               
                </select>

              </div> 


              <div class=" mb-3">
                <label for="telefone">Qnt. sessões</label>
                
                <input type="text" class="form-control" id="sessao" name="sessao" placeholder="Quantidade sessão" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>             
            
            

            <div class=" mb-3">
                <label for="Ativo">Instrutor</label>
                <input type="text" class="form-control" id="instrutor" name="instrutor" placeholder="Instrutor" required>
                </select>

            </div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Criar treino</button>
          </form>
        </div>

  </div></main>

  <footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
  
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>