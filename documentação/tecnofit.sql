-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 31-Jan-2021 às 22:49
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `tecnofit`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE `aluno` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `sexo` int(2) DEFAULT NULL,
  `cpf` varchar(11) NOT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `numero` int(5) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `informacoes` text DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`id`, `nome`, `email`, `telefone`, `sexo`, `cpf`, `endereco`, `numero`, `complemento`, `informacoes`, `created_at`) VALUES
(2, 'Alex Sandro de Moraes', 'alesan@gmail.com', '41992478214', 1, '1234567891', 'Alberto Otto', 1316, 'Blo 17', NULL, '2021-01-27 16:31:17'),
(5, 'Fernando Silva', 'alesan@gmail.com', '41992478214', 1, '1234556789', 'Alberto Otto', 1316, 'Bloc 17 apt 301', ' Condominio Cedros', '2021-01-27 16:31:13'),
(6, 'Joelma Lima Silva Alex', 'joelma@uol.com', '4199234567', 1, '12345678', 'Alexandre Salata', 123, 'frente', 'Nao tem ', '2021-01-28 10:39:04'),
(8, 'Lucimar Sandro', 'lucimar@gmail.com', '4128393839838', 2, '1234565767', 'ALberto Otto', 12, '', ' ', '2021-01-28 13:46:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `descricao_treino`
--

CREATE TABLE `descricao_treino` (
  `id` int(11) NOT NULL,
  `id_treino` int(11) NOT NULL,
  `id_exercicio` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `repeticao` int(4) NOT NULL,
  `ativo` int(1) NOT NULL,
  `finalizado` int(1) NOT NULL,
  `instrutor` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `descricao_treino`
--

INSERT INTO `descricao_treino` (`id`, `id_treino`, `id_exercicio`, `id_aluno`, `repeticao`, `ativo`, `finalizado`, `instrutor`, `created_at`) VALUES
(5, 2, 7, 8, 4, 1, 2, 'Lima Cunha', '2021-01-31 15:41:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `exercicio`
--

CREATE TABLE `exercicio` (
  `id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `information` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `exercicio`
--

INSERT INTO `exercicio` (`id`, `descricao`, `created_at`, `information`) VALUES
(2, 'Pular corda', '2021-01-28 14:48:51', ''),
(7, 'Parkour', '2021-01-31 15:12:55', ' teste'),
(8, 'Pernas', '2021-01-31 15:13:50', ' ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `treino`
--

CREATE TABLE `treino` (
  `id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `ativo` int(2) NOT NULL,
  `id_exercicio` int(2) NOT NULL,
  `information` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `treino`
--

INSERT INTO `treino` (`id`, `descricao`, `ativo`, `id_exercicio`, `information`, `created_at`) VALUES
(1, 'Pular corda', 1, 1, ' Pular 300 vezes', '2021-01-30 09:42:47'),
(2, 'Treino superior', 2, 2, ' ', '2021-01-31 10:07:59'),
(3, 'Treino completo', 2, 1, ' ', '2021-01-31 10:08:23'),
(4, 'Treino baixo', 1, 2, ' teste ', '2021-01-31 11:02:54');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `descricao_treino`
--
ALTER TABLE `descricao_treino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_treino` (`id_treino`),
  ADD KEY `id_exercicio` (`id_exercicio`),
  ADD KEY `id_aluno` (`id_aluno`);

--
-- Índices para tabela `exercicio`
--
ALTER TABLE `exercicio`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `treino`
--
ALTER TABLE `treino`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `aluno`
--
ALTER TABLE `aluno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `descricao_treino`
--
ALTER TABLE `descricao_treino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `exercicio`
--
ALTER TABLE `exercicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `treino`
--
ALTER TABLE `treino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `descricao_treino`
--
ALTER TABLE `descricao_treino`
  ADD CONSTRAINT `descricao_treino_ibfk_1` FOREIGN KEY (`id_treino`) REFERENCES `treino` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descricao_treino_ibfk_2` FOREIGN KEY (`id_exercicio`) REFERENCES `exercicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descricao_treino_ibfk_3` FOREIGN KEY (`id_aluno`) REFERENCES `aluno` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
