<?php
require "./model/dataBase.php";

abstract class dadosAluno {

public $conexao;
public $name;
public $email;
public $telefone;
public $sexo;
public $cpf;
public $nome;
public $endereco;
public $numero;
public $complemento;
public $information;
public $id;

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }
  public function setId($id) {
      $this->id = $id;
  }

  public function getEmail() {
      return $this->email;
  }

  public function setEmail($email) {
      $this->email = $email;
  }

  public function getTelefone() {
    return $this->telefone;
  }

  public function setTelefone($telefone) {
    $this->telefone = $telefone;
  }

  public function getSexo() {
      return $this->sexo;
  }

  public function setSexo($sexo) {
      $this->sexo = $sexo;
  }

  public function getCpf() {
    return $this->cpf;
  }

  public function setCpf($cpf) {
    $this->cpf = $cpf;
  }

  public function getEndereco() {
    return $this->endereco;
  }

  public function setEndereco($endereco) {
    $this->endereco = $endereco;
  }

  public function getNumero() {
    return $this->numero;
  }

  public function setNumero($numero) {
    $this->numero = $numero;
  }

  public function getComplemento() {
      return $this->complemento;
  }

  public function setComplemento($complemento) {
      $this->complemento = $complemento;
  }

  public function getInformation() {
    return $this->information;
  }

  public function setInformation($information) {
    $this->information = $information;
  }

  abstract protected function gravarRegistro(); 
  abstract protected function pesquisaRegistroAlunos(); 
  abstract protected function editarAluno();
  abstract protected function removerAluno(); 
  abstract protected function atualizaAluno();
  abstract protected function pesquisaAluno();

}

class alunoController extends dadosAluno {

  public function __construct(){
      
    $this->conexao = new Database();
  
  }

  public function gravarRegistro(){
    
    return $this->conexao->save($this);
  }


  public function pesquisaRegistroAlunos(){
    return $this->conexao->listAll();
  }

  public function editarAluno(){
    return $this->conexao->listAluno($this);
  }

  public function removerAluno(){
    return $this->conexao->apagarAluno($this);
  }

  public function atualizaAluno(){
   
    return $this->conexao->atualizarDadosAluno($this);
  }

  public function pesquisaAluno(){
   
    return $this->conexao->pesquisarAluno($this);
  }

}

?>