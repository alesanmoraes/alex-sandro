<?php
require "./model/dataBaseTreino.php";

abstract class dadosTreino {

public $conexao;
public $exercicio;
public $sessao;
public $repeticao;
public $instrutor;
public $id_exercicio; 
public $nome;
public $id_usuario;
public $ativo;
public $id_exec;
public $id_alu;
public $id_trei;
public $id_dest;
public $email;
public $telefone;
public $id;
public $information;


  public function getInformation() {
    return $this->information;
  }

  public function setInformation($information) {
    $this->information = $information;
  }
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
  }

  public function getTelefone() {
  return $this->telefone;
  }

  public function setTelefone($telefone) {
  $this->telefone = $telefone;
  }
  public function getId_exec() {
    return $this->id_exec;
  }

  public function setId_exec($id_exec) {
    $this->id_exec = $id_exec;
  }

  public function getId_alu() {
    return $this->id_alu;
  }

  public function setId_alu($id_alu) {
    $this->id_alu = $id_alu;
  }

  public function getId_trei() {
    return $this->id_trei;
  }

  public function setId_trei($id_trei) {
    $this->id_trei = $id_trei;
  }

  public function getId_dest() {
    return $this->id_dest;
  }

  public function setId_dest($id_dest) {
    $this->id_dest = $id_dest;
  }
  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }
  
  public function getAtivo() {
    return $this->ativo;
  }

  public function setAtivo($ativo) {
    $this->ativo = $ativo;
  }

  public function getExercicio() {
      return $this->exercicio;
  }

  public function setExercicio($exercicio) {
      $this->exercicio = $exercicio;
  }

  public function getSessao() {
    return $this->sessao;
  }

  public function setSessao($sessao) {
    $this->sessao = $sessao;
  }

  public function getRepeticao() {
      return $this->repeticao;
  }

  public function setRepeticao($repeticao) {
      $this->repeticao = $repeticao;
  }

  public function getInstrutor() {
    return $this->instrutor;
  }

  public function setInstrutor($instrutor) {
    $this->instrutor = $instrutor;
  }

  public function getId_exercicio() {
    return $this->id_exercicio;
  }

  public function setId_exercicio($id_exercicio) {
    $this->id_exercicio = $id_exercicio;
  }

  public function getId_usuario() {
    return $this->id_usuario;
  }

  public function setId_usuario($id_usuario) {
    $this->id_usuario = $id_usuario;
  }


  abstract protected function gravarTreino(); 
  abstract protected function pesquisaTreino(); 
  abstract protected function editarTreino();
  abstract protected function atualiza_treino(); 
  abstract protected function pesquisaTreinoAtivo();
  abstract protected function pesquisaTreinoDesativado();
  abstract protected function ativarTreino();
  abstract protected function criarTreino();
  abstract protected function buscarAllTreino();
  abstract protected function mostrarTreino();
  abstract protected function buscaAllTreinos();
  abstract protected function atualizaEditarTreino();
}

class treinoController extends dadosTreino {

  public function __construct(){
      
    $this->conexao = new DatabaseTreino();
  
  }

  public function gravarTreino(){
    
    return $this->conexao->save($this);
  }

  public function pesquisaTreino(){
    
    return $this->conexao->show($this);
  }

  public function editarTreino(){
    
    return $this->conexao->all($this);
  }

  public function atualiza_treino(){
    
    return $this->conexao->update($this);
  }

  public function pesquisaTreinoAtivo(){
    
    return $this->conexao->showAtivo($this);
  }

  public function pesquisaTreinoDesativado(){
    
    return $this->conexao->treinoDesativado($this);
  }

  public function ativarTreino(){
    
    return $this->conexao->ativaStatusTreino($this); 
  }
  
  public function criarTreino(){
    
    return $this->conexao->novoTreino($this); 
  }

  public function buscarAllTreino(){
    
    return $this->conexao->listAllTreino($this); 
  }

  public function buscaAllTreinos(){
    
    return $this->conexao->listAll($this); 
  }

  public function mostrarTreino(){
    
    return $this->conexao->carregaTreinos($this); 
  }

  public function atualizaEditarTreino(){
    
    return $this->conexao->atualizaTreino($this); 
  }

}

?>