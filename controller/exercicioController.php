<?php
require "./model/dataBaseExercicio.php";

abstract class dadosExercicio {

public $conexao;
public $name;
public $information;
public $id;


  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
      $this->id = $id;
  }

  public function getInformation() {
    return $this->information;
  }

  public function setInformation($information) {
    $this->information = $information;
  }

  abstract protected function gravarExercicio(); 
  abstract protected function pesquisaRegistroExercicio(); 
  abstract protected function editarExercicio();
  abstract protected function removerExercicio(); 
  abstract protected function atualizaExercicio();
  abstract protected function pesquisaExercicio();
  abstract protected function pesquisaExercicioAtivo();
  abstract protected function pesquisaTodosExercicio();

}

class exercicioController extends dadosExercicio {

  public function __construct(){
      
    $this->conexao = new DatabaseExercicio();
  
  }

  public function gravarExercicio(){
    
    return $this->conexao->save($this);
  }

  public function pesquisaRegistroExercicio(){
    return $this->conexao->listAll();
  }

  public function editarExercicio(){
    return $this->conexao->listExercicio($this);
  }

  public function atualizaExercicio(){
   
    return $this->conexao->atualizarDadosExercicio($this);
  }

  public function removerExercicio(){
    return $this->conexao->apagarExercicio($this);
  }
 
  public function pesquisaExercicio(){
    return $this->conexao->pesquisarExer($this);
  }

  public function pesquisaExercicioAtivo(){
    return $this->conexao->pesquisarExerAtivo($this); 
  }

  public function pesquisaTodosExercicio(){
    return $this->conexao->listAllExercicio($this); 
  }

}

?>