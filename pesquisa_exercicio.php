<?php

include_once './controller/exercicioController.php';
date_default_timezone_set('America/Sao_Paulo');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>
    <link href="./css/layout.css" rel="stylesheet">
  </head>
  <body>
   
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a class="navbar-brand" href="index.php">TecnoFit Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
        <div class="row">
          <div class="col-md-4">
          <h1>::ALUNO::</h1>
            <br/>
              <li><a href="cadastro_aluno.php"> Cadastrar aluno </a></li>
              <li><a href="editar_aluno.php"> Editar aluno</a></li>
              <li><a href="editar_aluno.php"> Remover aluno</a></li>
              <li><a href=""> Perfil aluno</a></li>
          </div>
          <div class="col-md-4">
            <h1>::EXERCÍCIOS::</h1>
            <br/>
              <li><a href="cadastrar_exercicio.php"> Cadastrar exercício </a></li>
              <li><a href="editar_exercicio.php"> Editar exercício </a></li>
              <li><a href="#"> Remover exercício</a></li>
          </div>
          <div class="col-md-4">
            <h1>::TREINO::</h1>
            <br/>
              <li><a href="cadastrar_treino.php"> Cadastrar treino </a></li>
              <li><a href=""> Editar treino</a></li>
              <li><a href=""> Ativar treino</a></li>
          </div>
        </div>
      </div>
      </div>

      <div class="container">
        
          <div class="col-md-12">

          
          <div class="col-md-10">

          <label for="email">Pesquisa Exercício</label>
             <form class="needs-validation" novalidate action="pesquisa_exercicio.php" method="POST">
            <div class="input-group">                
               
              <input type="text" class="form-control" name="nome_exercicio" id="nome_exercicio" placeholder="Descrição exercício">
              
              <button class="btn btn-primary " type="submit">Pesquisar</button>
              </form>
             
             </div>
          </div>
          


          <table class="table table-striped">
    <thead>
      <tr>
        <th>Código</th>
        <th>Descrição</th>            
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr>
<?php

if (!empty($_POST['nome_exercicio'])) {

$pesquisaAlunos = new exercicioController();   

$pesquisaAlunos->setName($_POST['nome_exercicio']);

$retorno = $pesquisaAlunos->pesquisaExercicio(); 
foreach ($retorno as $infoUser){     
    
  ?><td><?= $infoUser['id'];?></td>
  <td><?= $infoUser['descricao'];?></td>   
  <td><a class="btn btn-primary" href="visualizar_exercicio.php?id=<?= $infoUser['id'];?>">Visualizar</a> </td>
  <td><a class="btn btn-primary" href="visualizar_exercicio.php?id=<?= $infoUser['id'];?>">Atualizar</a> </td>
  <td><a class="btn btn-primary" href="removerExercicio.php?id=<?= $infoUser['id'];?>">Remover</a> </td>
  </tr> <?php

}
}
?>
</table>
          </div>         
        </div>

        <hr>

      </div> <!-- /container -->

    </main>
  


<footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
  
  </body>
</html>