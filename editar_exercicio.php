<?php

include_once './controller/exercicioController.php';
date_default_timezone_set('America/Sao_Paulo');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>
    <link href="./css/layout.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  </head>
  <body>
   
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a class="navbar-brand" href="index.php">TecnoFit Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
        <div class="row">
          <div class="col-md-4">
          <h1>:: ALUNO ::</h1>
            <br/>
            <li><a href="#" data-toggle="modal" data-target="#cadastrarAluno"> Cadastrar aluno </a></li>
              <li><a href="editar_aluno.php"> Editar aluno</a></li>
              <li><a href="editar_aluno.php"> Remover aluno</a></li>
              <li><a href="perfil_aluno.php"> Perfil aluno</a></li>
              <li><a href="escolha_aluno_treino.php"> Cadastrar exercício</a></li>
          </div>
          <div class="col-md-4">
            <div class="cor_font"><h1>:: EXERCÍCIOS ::</h1></div>
            <br/>
            <li><a href="#" data-toggle="modal" data-target="#cadastrarExercicio"> Cadastrar exercício </a></li>
              <li><a href="editar_exercicio.php"> Editar exercício </a></li>
              <li><a href="editar_exercicio.php"> Remover exercício</a></li>
          </div>
          <div class="col-md-4">
            <h1>:: TREINO ::</h1>
            <br/>
            <li><a href="#" data-toggle="modal" data-target="#cadastrarTreino"> Cadastrar treino </a></li>
              <li><a href="editar_treino.php" > Editar treino</a></li>
              <li><a href="#" data-toggle="modal" data-target="#ativarTreino"> Ativar treino</a></li>
          </div>
        </div>
      </div>
      </div>

      <div class="container">
        
          <div class="col-md-12">

          
          <div class="col-md-10">

          <label for="email"><div class="cor_desc">Pesquisa Exercício</div></label>
             <form class="needs-validation" novalidate action="pesquisa_exercicio.php" method="POST">
            <div class="input-group">                
               
              <input type="text" class="form-control" name="nome_exercicio" id="nome_exercicio" placeholder="Descrição exercício">
              
              <button class="btn btn-primary " type="submit">Pesquisar</button>
              </form>
             
             </div>
          </div>
          


          <table class="table table-striped">
    <thead>
      <tr>
        <th>Código</th>
        <th>Descrição</th>            
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr>
<?php

$pesquisaAlunos = new exercicioController();   

$retorno = $pesquisaAlunos->pesquisaRegistroExercicio(); 
foreach ($retorno as $infoUser){     
    
  ?><td><?= $infoUser['id'];?></td>
  <td><?= $infoUser['descricao'];?></td>   
  <td><a class="btn btn-primary" href="visualizar_exercicio.php?id=<?= $infoUser['id'];?>">Visualizar</a> </td>
  <td><a class="btn btn-primary" href="visualizar_exercicio.php?id=<?= $infoUser['id'];?>">Atualizar</a> </td>
  <td><a class="btn btn-primary" href="removerExercicio.php?id=<?= $infoUser['id'];?>">Remover</a> </td>
  </tr> <?php

}
?>
</table>
          </div>         
        </div>

        <hr>

      </div> <!-- /container -->

    </main>
  
  

<div id="cadastrarAluno" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal cadastrar aluno-->
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Cadastrar aluno</h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">            

          <form action="#" method="POST" id="cadAluno">
          <div class="mb-3">
                <label for="name">Nome</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                
              </div>            
            

            <div class="mb-3">
              <label for="email">E-mail</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</span>
                </div>
                <input type="text" class="form-control" id="email" name="email" placeholder="you@example.com" required>               
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="telefone">Telefone</label>
                
                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
               
              </div>
              <div class="col-md-6 mb-3">
                <label for="Sexo">Sexo</label>
                <select name="sexo" class="form-control">
                <option value="1" selected>Masculino</option>
                <option value="2">Feminino</option>
                </select>

              </div>
            </div>

            <div class="mb-3">
              <label for="cpf">Cpf</label>
              <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Cpf" maxlength="11" required>
              
            </div>

            <div class="mb-3">
              <label for="endereco">Endereço</label>
              <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" required>
              
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="numero">Número</label>
                <input type="text" class="form-control" id="numero" name="numero" placeholder="" required>
                
              </div>
              <div class="col-md-6 mb-3">
                <label for="complemento">Complemento</label>
                <input type="text" class="form-control" id="complemento" name="complemento" placeholder="" value="" required>
                
              </div>
            </div>

            <div class="mb-3">
              <label for="information">Outras informações</label>
              <textarea cols="50" rows="2" class="form-control" id="information" name="information" placeholder="Outras informações"> </textarea>
            </div>
              
          </div>         
          <div class="modal-footer">
           
            <button type="button" class="btn btn-primary" id="salvaAluno">Salvar</button>
            </form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
          </div>

        </div>
      </div>
    </div>


<div id="ativarTreino" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal ativar treino-->
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Ativar treino</h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">            

          <form action="#" method="POST" id="contactForm">

              <select name="treino" class="form-control">
                  <?php

                include_once './controller/treinoController.php';
               
                  $pesquisaTreino = new treinoController();   

                  $retornoTre = $pesquisaTreino->buscarAllTreino(); 

                  foreach ($retornoTre as $infoTreino){     
                      
                    ?><option value="<?= $infoTreino['id'];?>"><?= $infoTreino['descricao'];?></option>
                  <?php } ?>
               
              </select>
          </div>         
          <div class="modal-footer">
           
            <button type="button" class="btn btn-primary" id="salvar">Salvar</button>
            </form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
          </div>

        </div>
      </div>
    </div>

    <!--Cadastrar treino -->
    
<div id="cadastrarTreino" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Cadastrar treino</h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">            

          <form action="#" method="POST" id="editaForm">
            
              <div class="mb-3">
                <label for="name">Descrição</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                
              </div>

              <div class="mb-3">
                <label for="exercicio">Exercício</label>
                <select name="exercicio" class="form-control">
                  <?php
                 
                  include_once './controller/exercicioController.php';
                  
                  $pesquisaTreino = new exercicioController();   

                  $retorno = $pesquisaTreino->pesquisaTodosExercicio(); 


                  foreach ($retorno as $infoUser){     
                      
                    ?><option value="<?= $infoUser['id'];?>"><?= $infoUser['descricao'];?></option>
                  <?php

                  }
                  ?>
               
                </select>

              </div>

              <div class="mb-3">
                <label for="information">Outras informações</label>
                <textarea cols="50" rows="2" class="form-control" id="information" name="information" placeholder="Outras informações"> </textarea>
              </div>
          
          </div>         
          <div class="modal-footer">
           
            <button type="button" class="btn btn-primary" id="salvarTreino">Criar treino</button>
            </form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
          </div>

        </div>
      </div>
    </div>



    <!--Cadastrar exercicio -->
    
<div id="cadastrarExercicio" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Cadastrar exercício</h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">            

          <form action="#" method="POST" id="cadExercicio">
            
          <form class="needs-validation" novalidate action="criarExercicio.php" method="POST">
            
              <div class="mb-3">
                <label for="name">Descrição</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                
              </div>            
            

            <div class="mb-3">
              <label for="information">Outras informações</label>
              <textarea cols="50" rows="2" class="form-control" id="information" name="information" placeholder="Outras informações"> </textarea>
            </div>
                
          
          </div>         
          <div class="modal-footer">
           
            <button type="button" class="btn btn-primary" id="salvarExercicio">Criar exercício</button>
            </form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
          </div>

        </div>
      </div>
    </div>

<script>


// Ajax cadastro aluno
$(document).ready(function() {
  $('#salvaAluno').click(function(){
		event.preventDefault();
		var formDados = $('#cadAluno').serialize();

		$.ajax({                    
			url:'criarAluno.php',
			type:'POST',                        
			data:formDados,			
			success:function (data){

            alert("Cadastro com sucesso");            
            $('#meuModal').modal('hide'); // Fechar a modal 
            document.location.reload(true);                                   
			 
	    }
	  });			
	});
});

// Ajax cadastro exercicio
$(document).ready(function() {
  $('#salvarExercicio').click(function(){
		event.preventDefault();
		var formDados = $('#cadExercicio').serialize();

		$.ajax({                    
			url:'cadastrar_exercicio.php',
			type:'POST',                        
			data:formDados,			
			success:function (data){

            alert("Exercício cadastrado");            
            $('#meuModal').modal('hide'); // Fechar a modal 
            document.location.reload(true);                                   
			 
	    }
	  });			
	});
});

// Ajax ativa treino
$(document).ready(function() {
  $('#salvar').click(function(){
		event.preventDefault();
		var formDados = $('#contactForm').serialize();

		$.ajax({                    
			url:'ativa_treino_modal.php',
			type:'POST',                        
			data:formDados,			
			success:function (data){

            alert("Treino ativado");            
            $('#meuModal').modal('hide'); // Fechar a modal 
            document.location.reload(true);                                   
			 
	    }
	  });			
	});
});

// Ajax cadastrar treino
$(document).ready(function() {
  $('#salvarTreino').click(function(){
		event.preventDefault();
		var formDados = $('#editaForm').serialize();

		$.ajax({                    
			url:'criarTreino.php',
			type:'POST',                        
			data:formDados,			
			success:function (data){

            alert("Cadastro com sucesso");            
            $('#meuModal').modal('hide'); // Fechar a modal 
            document.location.reload(true);                                   
			 
	    }
	  });			
	});
});

</script>





<footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
  
  </body>
</html>