<?php

  class conexao {
    
    private $host = 'localhost';
    private $db_name = 'tecnofit';
    private $username = 'root';
    private $password = '';
 
    public $pdo;

    // DB Connect
    public function __construct() {
     

      try {
        $this->pdo = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
      }

      return $this->pdo;
    }
  }