<?php

include_once 'conexao.php';

  class DatabaseTreino extends conexao {
    
    public $conn;
     
     public function __construct(){
     
      $this->conn = new conexao();
     
     }

     public function save($arq){
            
      $exer = $arq->getId_exercicio();
      $sessao = $arq->getSessao();  
      $instrutor = $arq->getInstrutor();          
      $usuario = $arq->getId_usuario();
      $treino = $arq->getId_trei();
      $ativo = 1;
      $finaliza = 2;

            
      $sql = "INSERT INTO descricao_treino (id_treino, id_exercicio, id_aluno, repeticao, ativo, finalizado, instrutor, created_at) VALUES (?,?,?,?,?,?,?,NOW())";

      $query = $this->conn->pdo->prepare($sql);        
       
      $query->bindParam(1, $treino);     
      $query->bindParam(2, $exer);
      $query->bindParam(3, $usuario); 
      $query->bindParam(4, $sessao);
      $query->bindParam(5, $ativo);
      $query->bindParam(6, $finaliza);
      $query->bindParam(7, $instrutor);
      

       $query->execute();

   }


   public function show($arq){

    $nomePesq = $arq->getNome();
      
    $search = $nomePesq.'%';
     
    $sql = "SELECT exer.id as cod_exer, alu.id as cod_alu, alu.nome as nome, tre.id as cod_trei, tre.information, dest.id as cod_dest, exer.descricao as descricao_exer,tre.descricao as descricao_treino, dest.instrutor as instrutor,
            dest.ativo FROM descricao_treino as dest INNER JOIN treino as tre on tre.id = dest.id_treino
            INNER JOIN aluno as alu on dest.id_aluno = alu.id INNER JOIN exercicio as exer on dest.id_exercicio = exer.id
            WHERE alu.nome LIKE :nome GROUP BY alu.id ORDER BY dest.id desc";

    $query = $this->conn->pdo->prepare($sql);
    $query->bindParam(':nome', $search);
  
    $query->execute();
    
      $row = $query->fetchAll();
      
      return $row;
  
  }


  public function all($arq){

    $id_exec = $arq->getId_exec();
    $id_alu = $arq->getId_alu();
    $id_trei = $arq->getId_trei();
    $id_dest = 1;
       
    $sql = "SELECT exer.descricao, exer.information, alu.nome,alu.email,alu.telefone, 
            tre.descricao as treino_alu, dest.instrutor, tre.ativo, dest.repeticao, dest.ativo , dest.finalizado FROM 
            exercicio as exer 
            INNER JOIN descricao_treino as dest on exer.id = dest.id_exercicio
	          INNER JOIN treino as tre on dest.id_treino = tre.id
	          INNER JOIN aluno as alu on dest.id_aluno = alu.id
            WHERE dest.id_exercicio = ? and dest.id_aluno = ? and dest.id_treino = ? 
            GROUP BY alu.id";


    $query = $this->conn->pdo->prepare($sql);
    

    $query->bindParam('1', $id_exec);
    $query->bindParam('2', $id_alu);
    $query->bindParam('3', $id_trei);
    //$query->bindParam('4', $id_dest);
  
    $query->execute();
    
      $row = $query->fetchAll();
      
      return $row;
  
  }


  
 public function update($arq){ 
  
  $sessao = $arq->getSessao(); 
  $repet = $arq->getRepeticao();  
  $instru = $arq->getInstrutor();
  $ativo = $arq->getAtivo(); 
  $id_exer = $arq->getId_exec(); //2
  $id_aluno = $arq->getId_alu(); //9
  $id_trei = $arq->getId_trei();

   $sql = "UPDATE descricao_treino SET repeticao = ?, ativo = ?, finalizado = ? , instrutor = ? WHERE id_treino = ? AND id_exercicio = ? AND id_aluno = ?";
            
   $query = $this->conn->pdo->prepare($sql);        
            
    $query->bindParam(1, $repet);
    $query->bindParam(2, $ativo);
    $query->bindParam(3, $sessao);
    $query->bindParam(4, $instru);
    $query->bindParam(5, $id_trei);
    $query->bindParam(6, $id_exer);
    $query->bindParam(7, $id_aluno);
                
   $query->execute();

   $this->updateEXercicio($arq);
                       
}


public function updateEXercicio($arq){   

  $exer = $arq->getExercicio();
  $id_exer = $arq->getId_exec();
       
 $sql = "UPDATE exercicio SET descricao = ?  WHERE id = ?";
            
   $query = $this->conn->pdo->prepare($sql);        
            
    $query->bindParam(1, $exer);
    $query->bindParam(2, $id_exer);     
            
   $query->execute();                       
}

public function showAtivo($arq){

  $nomePesq = $arq->getNome();
    
  $search = $nomePesq.'%';
   
  $sql = "SELECT exer.id as cod_exer, alu.id as cod_alu, tre.id as cod_trei,dest.id as cod_dest, exer.descricao as descricao_exer ,alu.nome as nome,tre.descricao as descricao_treino, tre.instrutor as instrutor, dest.repeticao
          FROM exercicio as exer inner join treino as tre on tre.id_aluno inner join descricao_treino as dest on dest.id_exercicio = tre.id_aluno inner join aluno as alu on alu.id = tre.id_aluno
          WHERE alu.nome LIKE :nome AND tre.ativo = 1 group by alu.id"; 

  $query = $this->conn->pdo->prepare($sql);
  $query->bindParam(':nome', $search);

  $query->execute();
  
    $row = $query->fetchAll();
    
    return $row;

}


public function treinoDesativado($arq){

  $nomePesq = $arq->getNome();
    
  $search = $nomePesq.'%';
   
  $sql = "SELECT exer.id as cod_exer, alu.id as cod_alu, tre.id as cod_trei,dest.id as cod_dest, exer.descricao as descricao_exer ,alu.nome as nome,tre.descricao as descricao_treino, tre.instrutor as instrutor, dest.repeticao
          FROM exercicio as exer inner join treino as tre on tre.id_aluno inner join descricao_treino as dest on dest.id_exercicio = tre.id_aluno inner join aluno as alu on alu.id = tre.id_aluno
          WHERE alu.nome LIKE :nome and tre.ativo = 2 group by alu.id";

  $query = $this->conn->pdo->prepare($sql);
  $query->bindParam(':nome', $search);

  $query->execute();
  
    $row = $query->fetchAll();
    
    return $row;

}


public function ativaStatusTreino($arq){ 
  

 $treino = $arq->getId_trei();  
 $ativa = 1;
 
 $sql = "UPDATE treino SET ativo = ? WHERE id = ?";
            
   $query = $this->conn->pdo->prepare($sql);        
            
    $query->bindParam(1, $ativa);
    $query->bindParam(2, $treino);
   
    $query->execute();   
    
    $this->updateTreinoStatus($arq);
}



public function updateTreinoStatus($arq){ 

  $id = $arq->getId_dest();
  $status = 1;
  $finaliza = 2;    
     
  $sql = "UPDATE descricao_treino SET status_treino = ?, finalizado = ?  WHERE id = ?";
            
   $query = $this->conn->pdo->prepare($sql);        
            
    $query->bindParam(1, $status);
    $query->bindParam(2, $finaliza);
    $query->bindParam(3, $id); 
            
   $query->execute();                       
}

public function novoTreino($arq){

  $exer = $arq->getExercicio(); 
  $nome = $arq->getNome();  
  $atividade = $arq->getAtivo(); 
  $status = 1;

  $sql = "INSERT INTO treino (descricao,ativo,id_exercicio,information,created_at) VALUES (?,?,?,?,NOW())";

     $query = $this->conn->pdo->prepare($sql);        
      
     $query->bindParam(1, $nome); 
     $query->bindParam(2, $status);    
     $query->bindParam(3, $exer);
     $query->bindParam(4, $atividade);     
     

      $query->execute();

 }

 public function listAllTreino() {
   
  $sql = "SELECT id,descricao,ativo,id_exercicio,information FROM treino WHERE ativo = 2 ORDER BY id DESC";
     $query = $this->conn->pdo->prepare($sql);
    
     $query->execute();
     
       $row = $query->fetchAll();
       
       return $row;

}

public function listAll() {
   
  $sql = "SELECT id,descricao,ativo,id_exercicio,information FROM treino ORDER BY id DESC";
     $query = $this->conn->pdo->prepare($sql);
    
     $query->execute();
     
       $row = $query->fetchAll();
       
       return $row;

}

public function carregaTreinos($arq) {
  
  $codigo = $arq->getId_trei(); 
  
  $sql = "SELECT tre.id, tre.descricao,tre.ativo, tre.information, tre.id_exercicio, exer.descricao as exercicio from
          treino as tre INNER JOIN exercicio as exer on exer.id = tre.id_exercicio where tre.id = ?";

  $query = $this->conn->pdo->prepare($sql);
    
  $query->bindParam(1, $codigo); 

    $query->execute();
     
    $row = $query->fetchAll();
    
    return $row;
  
}


public function atualizaTreino($arq){ 
  

  $id = $arq->getId();
  $nome = $arq->getNome(); 
  $exercicio = $arq->getExercicio();
  $ativo = $arq->getAtivo();
  $info = $arq->getInformation();
  
  $sql = "UPDATE treino SET descricao = ?, ativo = ? , id_exercicio = ?, information = ? WHERE id = ?";
             
    $query = $this->conn->pdo->prepare($sql);        
             
     $query->bindParam(1, $nome);
     $query->bindParam(2, $ativo);
     $query->bindParam(3, $exercicio);
     $query->bindParam(4, $info);
     $query->bindParam(5, $id); 
     
    
     $query->execute();   
     
 }



  }
