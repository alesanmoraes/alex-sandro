<?php

include_once 'conexao.php';

  class DatabaseExercicio extends conexao {
    
    public $conn;
     
     public function __construct(){
     
      $this->conn = new conexao();
     
     }

     public function save($arq){
      
      $name = $arq->getName();      
      $inf = $arq->getInformation();
          
       
      $sql = "INSERT INTO exercicio (descricao,information,created_at) VALUES (?,?,NOW())";


      $query = $this->conn->pdo->prepare($sql);        
       
      $query->bindParam(1, $name);     
      $query->bindParam(2, $inf);
       
       $query->execute();

       
   }

   public function listAll() {
   
    $sql = "SELECT id,descricao FROM exercicio ORDER BY id DESC LIMIT 10";
       $query = $this->conn->pdo->prepare($sql);
      
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;

   }


   public function listExercicio($arq) {

    $id = $arq->getId();

    $sql = "SELECT id,descricao,information FROM exercicio WHERE id = ?";
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam('1', $id);
      
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
   }

   public function atualizarDadosExercicio($arq){
  
    $cod = $arq->getId();
    $nome = $arq->getName();    
    $information = $arq->getInformation();
    

   
     $sql = "UPDATE exercicio SET descricao = ?, information = ? WHERE id = ?";
              
     $query = $this->conn->pdo->prepare($sql);        
              
      $query->bindParam(1, $nome);      
      $query->bindParam(2, $information);
      $query->bindParam(3, $cod);
              
     $query->execute();
                        
  }

  public function apagarExercicio($arq){
        
    $cod = $arq->getId();
    
     $sql = "DELETE FROM exercicio WHERE id = ?";
              
     $query = $this->conn->pdo->prepare($sql);        
              
     $query->bindParam('1',$cod);
              
     $query->execute();
              
 }


 public function pesquisarExer($arq){

  $nomePesq = $arq->getName();
    
  $search = $nomePesq.'%';
  
  $sql = "SELECT id, descricao, information FROM exercicio WHERE descricao LIKE :nome";
  $query = $this->conn->pdo->prepare($sql);
  $query->bindParam(':nome', $search);

  $query->execute();
  
    $row = $query->fetchAll();
    
    return $row;

}

public function pesquisarExerAtivo($arq) {

  $id = $arq->getId();
  $status = 1;

  $sql = "SELECT alu.nome FROM exercicio as exerc INNER JOIN descricao_treino as descr on descr.id_exercicio = exerc.id 
          INNER JOIN aluno as alu on descr.id_aluno = alu.id
          WHERE descr.ativo = ? and descr.id_exercicio = ?";

 
     $query = $this->conn->pdo->prepare($sql);
     $query->bindParam('1', $status);
     $query->bindParam('2', $id);
    
     $query->execute();
     
       $row = $query->fetchAll();
       
       return $row;
 }


public function listAllExercicio() {
   
  $sql = "SELECT id,descricao FROM exercicio ORDER BY id DESC";
     $query = $this->conn->pdo->prepare($sql);
    
     $query->execute();
     
       $row = $query->fetchAll();
       
       return $row;

}



  }
