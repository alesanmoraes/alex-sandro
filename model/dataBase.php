<?php

include_once 'conexao.php';

  class Database extends conexao {
    
    public $conn;
     
     public function __construct(){
     
      $this->conn = new conexao();
     
     }

     public function save($arq){
      
      $name = $arq->getName();
      $email = $arq->getEmail();
      $telefone = $arq->getTelefone();
      $sexo = $arq->getSexo();
      $cpf = $arq->getCpf();
      $endereco = $arq->getEndereco();
      $numero = $arq->getNumero();
      $complemento = $arq->getComplemento();
      $information = $arq->getInformation();
          
       
      $sql = "INSERT INTO aluno (nome,email,telefone,sexo,cpf,endereco,numero,complemento,informacoes,
               created_at) VALUES (?,?,?,?,?,?,?,?,?,NOW())";


      $query = $this->conn->pdo->prepare($sql);        
       
      $query->bindParam(1, $name);
      $query->bindParam(2, $email);
      $query->bindParam(3, $telefone);
      $query->bindParam(4, $sexo);
      $query->bindParam(5, $cpf);
      $query->bindParam(6, $endereco);
      $query->bindParam(7, $numero);
      $query->bindParam(8, $complemento);
      $query->bindParam(9, $information);
       
       $query->execute();

       
   }



  public function listAll() {
   
    $sql = "SELECT id,nome,email,telefone,sexo,cpf,endereco,numero,complemento,informacoes FROM aluno ORDER BY id DESC LIMIT 10";
       $query = $this->conn->pdo->prepare($sql);
      
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
   }

   public function listAluno($arq) {

    $id = $arq->getId();

    $sql = "SELECT nome,email,telefone,sexo,cpf,endereco,numero,complemento,informacoes FROM aluno WHERE id = ?";
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam('1', $id);
      
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
   }

   public function apagarAluno($arq){
        
    $cod = $arq->getId();
    
     $sql = "DELETE FROM aluno WHERE id = ?";
              
     $query = $this->conn->pdo->prepare($sql);        
              
     $query->bindParam('1',$cod);
              
     $query->execute();
              
 }

 public function atualizarDadosAluno($arq){
  
  $cod = $arq->getId();
  $name = $arq->getName();
  $email = $arq->getEmail();
  $telefone = $arq->getTelefone();
  $sexo = $arq->getSexo();
  $cpf = $arq->getCpf();
  $endereco = $arq->getEndereco();
  $numero = $arq->getNumero();
  $complemento = $arq->getComplemento();
  $information = $arq->getInformation();
  
  
   $sql = "UPDATE aluno SET nome = ?, email = ?, telefone = ?, sexo = ?, cpf = ?, endereco = ?, numero = ?,
           complemento = ?, informacoes = ?  WHERE id = ?";
            
   $query = $this->conn->pdo->prepare($sql);        
            
    $query->bindParam(1, $name);
    $query->bindParam(2, $email);
    $query->bindParam(3, $telefone);
    $query->bindParam(4, $sexo);
    $query->bindParam(5, $cpf);
    $query->bindParam(6, $endereco);
    $query->bindParam(7, $numero);
    $query->bindParam(8, $complemento);
    $query->bindParam(9, $information);
    $query->bindParam(10, $cod);
            
   $query->execute();
                       
}

public function pesquisarAluno($arq){

  $nomePesq = $arq->getNome();
    
  $search = $nomePesq.'%';
  
  $sql = "SELECT id, nome, email, telefone, sexo, cpf FROM aluno WHERE nome LIKE :nome";
  $query = $this->conn->pdo->prepare($sql);
  $query->bindParam(':nome', $search);

  $query->execute();
  
    $row = $query->fetchAll();
    
    return $row;

}



  }
