<!doctype html>
<html lang="pt-br">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>

    <link href="./css/layout.css" rel="stylesheet">
  </head>
  <body class="bg-light">

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="index.php">TecnoFit Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      </div>
    </nav>

    <main role="main" class="container">

  <div class="position-relative p-1 p-md-5 m-md-3">
    <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Editar treino</h4>
         
          <?php
                
                include_once './controller/treinoController.php';
                
                $pesquisaTreino = new treinoController();   

                $pesquisaTreino->setId_trei($_POST['treino']);

                $retorno = $pesquisaTreino->mostrarTreino(); 
                
                foreach ($retorno as $infoUser){ ?>

          <form class="needs-validation" novalidate action="atualizaTreinoAtivo.php" method="POST">
            <input type="hidden" name="id" value="<?=$infoUser['id']?>" />
         
              <div class="mb-3">
                <label for="name">Descrição</label>
                <input type="text" class="form-control" id="name" name="name" placeholder=""  value="<?= $infoUser['descricao'];?>" required>
                
              </div>

              <div class="mb-3">
              
                <label for="exercicio">Exercício</label>
                
                <select name="exercicio" class="form-control">                      
                      
                    <option value="<?= $infoUser['id_exercicio'];?>"><?= $infoUser['exercicio'];?></option>
                  <?php

                  }
                  ?>
               
                </select>

              </div>

              <div class="mb-3">
                <label for="ativo">Status</label>
                
                <?php if ($infoUser['ativo'] == 1){
                  $descAtivo = "Ativo";
                }else{
                  $descAtivo = "Desativado";
                }
                ?>
                <select name="ativo" class="form-control">                   
                      
                    <option value="<?= $infoUser['ativo'];?>"><?= $descAtivo;?></option>
                    <option value="1">Ativo</option>              
                    <option value="2">Desativado</option>              
               
                </select>
              </div>

              <div class="mb-3">
                <label for="information">Outras informações</label>
                <textarea cols="50" rows="2" class="form-control" id="information" name="information" placeholder="Outras informações"><?= $infoUser['information'];?> </textarea>
              </div>

            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Atualizar</button>
          </form>
        </div>

  </div></main>

  <footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
  
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>