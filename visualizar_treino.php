<?php
include_once './controller/treinoController.php';
?>
<!doctype html>
<html lang="pt-br">
  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Academia TecnoFit</title>

    <link href="./css/layout.css" rel="stylesheet">
  </head>
  <body class="bg-light">

  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="index.php">TecnoFit Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      </div>
    </nav>

    <main role="main" class="container">

  <div class="position-relative p-1 p-md-5 m-md-3">
    <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Informações treino aluno</h4>
          
<?php    


$pesquisaTreino = new treinoController();   

$pesquisaTreino->setId_exec($_GET['cod_exer']);
$pesquisaTreino->setId_alu($_GET['cod_alu']); 
$pesquisaTreino->setId_trei($_GET['cod_trei']); 


$retorno = $pesquisaTreino->editarTreino(); 
foreach ($retorno as $infoUser){ 
?>  

              <form class="needs-validation" novalidate action="atualiza_treino.php" method="POST">
              <input type="hidden" name="cod_dest" value="<?=$_GET['cod_dest']?>" />
              <input type="hidden" name="cod_trei" value="<?=$_GET['cod_trei']?>" />

              <input type="hidden" name="cod_exer" value="<?=$_GET['cod_exer']?>" />
              <input type="hidden" name="cod_alu" value="<?=$_GET['cod_alu']?>" />
              
              <div class="mb-3">
                <label for="name">Nome</label>
                <input type="text" disabled class="form-control" id="name" name="name" value="<?=$infoUser['nome']?>" placeholder="" required>
                
              </div>            
            

            <div class="mb-3">
              <label for="email">E-mail</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</span>
                </div>
                <input type="text" disabled class="form-control" id="email" name="email" value="<?=$infoUser['email']?>" placeholder="you@example.com" required>               
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="telefone">Telefone</label>
                
                <input type="text" disabled class="form-control" id="telefone" name="telefone"  value="<?=$infoUser['telefone']?>" placeholder="Telefone" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="Sexo">Sexo</label>
                <select name="sexo" class="form-control" disabled>

              <?php print $infoUser['sexo'];
                if ($infoUser['sexo'] == 1) {
                  $desc = "Masculino";
                } else {
                  $desc = "Feminino";
                }
              ?>
                <option value="<?=$infoUser['sexo']?>" selected><?=$desc?></option>
                <option value="1">Masculino</option>
                <option value="2">Feminino</option>                
                </select>

              </div>
            </div>

            <div class="mb-3">
              <label for="Exercicio">Típo de exercício</label>
              <input type="text" class="form-control" disabled id="Exercicio" name="Exercicio" value="<?=$infoUser['descricao']?>" placeholder="1234 Main St" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="treino">Treino</label>
              <input type="text" class="form-control" id="treino" name="treino" value="<?=$infoUser['treino_alu']?>" placeholder="Treino" required>
             
            </div>

            <div class="row">
              
              <div class="col-md-6 mb-3">
                <label for="status_treino">Exercício</label>

                <?php  
                $treino = ($infoUser['ativo'] == 1) ? "Andamento" : "Finalizado";
                
                if ($infoUser['ativo'] == 1) { ?> 
                    <select name="status_treino" class="form-control">
                    <option value="<?=$infoUser['ativo']?>"><?=$treino?></option>
                    <option value="1">Andamento</option>
                    <option value="2">Finalizado</option> 
                <?php } else { ?>
                    <select name="status_treino" class="form-control" disabled> 
                    <option value="<?=$infoUser['ativo']?>" ><?=$treino?></option>
                <?php }  ?>
                    </select>

                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>

              
              <div class="col-md-6 mb-3">
                <label for="finalizado">Pular exercício</label>

                <?php 
                $pular_exer = ($infoUser['finalizado'] == 1) ? "Sim" : "Não";
                
                if ($infoUser['ativo'] == 1) { ?>
                  <select name="finalizado" class="form-control">
                  <option value="<?=$infoUser['finalizado']?>" selected><?=$pular_exer?></option>
                  <option value="1">Sim</option>
                  <option value="2">Não</option> 
                <?php } else { ?>   
                  <select name="finalizado" class="form-control" disabled>  
                  <option value="<?=$infoUser['finalizado']?>" ><?=$pular_exer?></option>  
                <?php }  ?>       
                </select>

              </div>
            </div>

            <div class="row">
              <div class="col-md-4 mb-4">
               <label for="repeticao">Sessão</label>
                <input type="text" class="form-control" id="repeticao" name="repeticao" value="<?=$infoUser['repeticao']?>" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              <div class="col-md-8 mb-2">
                <label for="instrutor">Instrutor</label>
                <input type="text" class="form-control" id="instrutor" name="instrutor" value="<?=$infoUser['instrutor']?>" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              </div>

            <div class="mb-3">
              <label for="information">Outras informações</label>
              <textarea cols="50" rows="2" class="form-control" disabled id="information" name="information" placeholder="Outras informações"><?=$infoUser['information']?></textarea>
            </div>
         <?php } ?>

         <hr class="mb-8">

            <div class="row">
            <div class="col-md-3">
            
            <button class="btn btn-primary" type="submit">Atualizar</button>
            </form>
            </div>
           
        </div>

  
            

  </div></main>

  <footer class="blog-footer bg-dark "> <div class="color_footer">&copy; 2021 TecnoFit</div></footer>
  
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>